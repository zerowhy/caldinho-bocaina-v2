# Changelog
### v1.2.5
Trocar para tipo comum de pedido depois de registrar pedido.
Trocar string para raw no regex

### v1.2.4
Arrumar erro de matemática básica (somar descontos e não subtrair descontos)

### v1.2.3
Arrumar bug onde após chegar em um número com dois dígitos no ClienteID o programa não abria.

### v1.2.2
Arrumar contador de semana e mês (-1 dia)

### v1.2.1
Criar automaticamente a base de dados caso ela não exista.

## v1.2
Arrumar MUITOS bugs e grandes melhoras para a página de estatísticas: entrada manual da renda do Uber Eats e Ifood (devido as taxas inconsistentes)

### v1.1.1
Arrumar bug onde a tabela na tela de estatísticas não era limpa ao atualizar.

## v1.1
Foi adicionado uma nova página: estatísticas.

## v1.0
Versão inicial
