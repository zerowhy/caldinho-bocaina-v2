# Author: Lucas Santos
# GUI software configuration to work alongside MainWindow.py (QtCreator python file)

# LICENSE
# This file is part of caldinho-bocaina-v2.
#
#    caldinho-bocaina-v2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    caldinho-bocaina-v2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with caldinho-bocaina-v2.  If not, see <https://www.gnu.org/licenses/>.


import sys
from os import path
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from MainWindow import Ui_MainWindow
from CustomWidget import Ui_Form
from sqlite_interface import *
from cep_data import *
from datetime import date, timedelta
import signal


class DynamicWidget(QWidget, Ui_Form):
    def __init__(self, info, *args, **kwargs):
        super(DynamicWidget, self).__init__(*args, **kwargs)
        self.setupUi(self)
        self.setFixedHeight(400)
        self.b_finalizar.clicked.connect(self.conclude)

        self.l_nome_cliente.setText(info["Nome"])
        self.l_tipo.setText(info["Tipo"])
        self.l_appid.setText(info["AppID"])
        self.l_desconto.setText(info["Desconto"])
        self.l_valor_final.setText(info["ValorFinal"])
        self.l_tel.setText(info["Telefone"])
        self.l_cep.setText(info["CEP"])
        self.l_rua.setText(info["Rua"])
        self.l_num.setText(info["Numero"])
        self.l_bairro.setText(info["Bairro"])
        self.l_cidade.setText(info["Cidade"])
        self.pedidoid = info["PedidoID"]
        self.add_items(info["Itens"])

    def add_items(self, lst):
        for cont in lst:
            for n in range(0, cont[2]):
                self.lw_itens.addItem(cont[0])
                self.lw_observacoes.addItem(cont[1])

    def conclude(self):
        conclude_pedido(database_file, self.pedidoid)
        window.update_pedido_tree()
        self.deleteLater()


class StandardItem(QStandardItem):
    def __init__(self, txt='', editable=False):
        super().__init__()
        self.setText(str(txt))
        self.setEditable(editable)


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)
        create_database(database_file)

        # Software version and GitLab link
        self.gitlab_link = 'https://gitlab.com/zerowhy/caldinho-bocaina-v2'
        self.version_number = 'v1.2.5'

        self.gitlab.setText(f'<a href="{self.gitlab_link}">GitLab</a>')
        self.gitlab.setOpenExternalLinks(True)
        self.version.setText(self.version_number)

        # Set Window Icon
        self.setWindowIcon(QIcon('images/icon.png'))

        # Show maximized app by default
        self.showMaximized()

        # Tab 1

        # tableWidget config
        self.update_table()
        self.tableWidget.setHorizontalHeaderLabels(['Primeiro Nome', 'Ultimo Nome', 'Telefone',
                                                    'CEP', 'Rua', 'Número', 'Bairro', 'Cidade'])
        self.tableWidget.setColumnWidth(0, 125)
        self.tableWidget.setColumnWidth(4, 200)
        self.tableWidget.setColumnWidth(6, 150)

        # Validate input on lineEdit
        reg_ex2 = QRegExp(r"^[0-9]{1,14}$")
        input_validator2 = QRegExpValidator(reg_ex2, self.lineEdit)
        self.lineEdit.setValidator(input_validator2)

        # Button clicked events
        self.pushButton_4.clicked.connect(self.get_cep)
        self.pushButton.clicked.connect(self.add_client)
        self.btn_refresh_client.clicked.connect(self.update_table)
        self.btn_atualizar_cliente.clicked.connect(self.update_cliente)
        self.btn_remove_client.clicked.connect(self.remove_cliente)

        # Validade input on lineEdit_2
        reg_ex1 = QRegExp(r"[0-9]{1,8}$")
        input_validator1 = QRegExpValidator(reg_ex1, self.lineEdit_2)
        self.lineEdit_2.setValidator(input_validator1)

        # Tab 1 end

        # Tab 2
        # Button clicked events
        self.pushButton_3.clicked.connect(self.add_to_list)
        self.pushButton_5.clicked.connect(self.remove_from_list)
        self.pushButton_6.clicked.connect(self.clear_list)
        self.pushButton_2.clicked.connect(self.add_order)
        self.pushButton_7.clicked.connect(self.update_field_clientes)

        # Radio clicked events
        self.current_radio = 'Comum'
        self.radioButton.toggled.connect(self.radio_clicked)
        self.radioButton_2.toggled.connect(self.radio_clicked)
        self.radioButton_3.toggled.connect(self.radio_clicked)

        # Validate input on lineEdit_7
        reg_ex = QRegExp(r"[0-9]+(\.|,)?[0-9]{,2}")
        input_validator = QRegExpValidator(reg_ex, self.lineEdit_7)
        self.lineEdit_7.setValidator(input_validator)

        # Add content to comboboxes
        self.update_field_clientes()
        self.update_field_cardapio()

        # Tab 2 end

        # Tab 3

        # Add all ongoing orders to Dynamic View.
        self.add_all_current_pedidos()

        # Setup tree view
        self.update_pedido_tree()
        header_view = QHeaderView(Qt.Horizontal)
        header_view.setVisible(False)
        header_view.setSectionHidden(0, True)
        header_view.setDefaultSectionSize(300)
        self.treeView.setHeader(header_view)

        # Tab 4
        self.tabWidget.currentChanged.connect(self.tab_changed)
        self.tb_vendidos_d.setColumnWidth(0, 220)
        self.tb_vendidos_w.setColumnWidth(0, 220)
        self.tb_vendidos_m.setColumnWidth(0, 220)

        self.tb_vendidos_d.setColumnWidth(1, 50)
        self.tb_vendidos_w.setColumnWidth(1, 50)
        self.tb_vendidos_m.setColumnWidth(1, 50)

        self.update_statistics()

        reg_ex = QRegExp(r"[0-9]+(\.|,)?[0-9]{,2}")
        input_validator = QRegExpValidator(reg_ex, self.l_renda_ifood)
        self.l_renda_ifood.setValidator(input_validator)

        reg_ex = QRegExp(r"[0-9]+(\.|,)?[0-9]{,2}")
        input_validator = QRegExpValidator(reg_ex, self.l_renda_uber)
        self.l_renda_uber.setValidator(input_validator)

        self.btn_add_renda.clicked.connect(self.add_app_income)

    def tab_changed(self):
        if self.tabWidget.currentIndex() == 3:
            self.update_statistics()

    def add_app_income(self):
        renda_uber = self.l_renda_uber.text().replace(',', '.')
        renda_ifood = self.l_renda_ifood.text().replace(',', '.')

        data_uber = {
            "Tipo": "Uber Eats",
            "Valor": renda_uber
        }

        data_ifood = {
            "Tipo": "Ifood",
            "Valor": renda_ifood
        }

        # print(data_uber, data_ifood)

        if renda_uber != '':
            add_row(database_file, 'renda_apps', data_uber)
        if renda_ifood != '':
            add_row(database_file, 'renda_apps', data_ifood)

        self.l_renda_uber.setText("")
        self.l_renda_ifood.setText("")
        self.update_statistics()

    def update_statistics(self):

        # Reset table
        self.tb_vendidos_d.setRowCount(0)
        self.tb_vendidos_w.setRowCount(0)
        self.tb_vendidos_m.setRowCount(0)

        # Main variables
        pedidos = get_pedido_data(database_file)
        valor_total_d, valor_total_w, valor_total_m = 0, 0, 0
        valor_total_d_uber, valor_total_w_uber, valor_total_m_uber = 0, 0, 0
        valor_total_d_ifood, valor_total_w_ifood, valor_total_m_ifood = 0, 0, 0
        discount_d = 0
        discount_w = 0
        discount_m = 0

        # Date variables
        today = date.today()
        past_week = today - timedelta(days=6)
        past_month = today - timedelta(days=29)
        l_today = today.strftime("%d/%m/%Y")
        l_past_week = past_week.strftime(f"%d/%m/%Y - {l_today}")
        l_past_month = past_month.strftime(f"%d/%m/%Y - {l_today}")

        # Set date labels
        self.l_data_d.setText(l_today)
        self.l_data_w.setText(l_past_week)
        self.l_data_m.setText(l_past_month)

        # Dates for database querying
        dates = []
        for a in range(0, 30):
            dates.append((today - timedelta(days=a)).strftime("%Y-%m-%d"))

        # Parse values from database
        today_itens = []
        today_itens_uber = []
        today_itens_ifood = []
        week_itens = []
        week_itens_uber = []
        week_itens_ifood = []
        month_itens = []
        month_itens_uber = []
        month_itens_ifood = []

        for pedido in pedidos:
            pedidoid = pedido[1]
            pedido_date = pedido[9][:10]
            itens = get_itens_pedido(database_file, pedidoid)
            item_lst = []
            item_lst_uber = []
            item_lst_ifood = []
            for item in itens:
                quantidade = item[2]
                for a in range(quantidade):
                    item_lst.append(get_cardapio_nome_id(database_file, item[1]))
                    if pedido[6] == "Ifood":
                        item_lst_ifood.append(get_cardapio_nome_id(database_file, item[1]))
                    elif pedido[6] == "Uber Eats":
                        item_lst_uber.append(get_cardapio_nome_id(database_file, item[1]))

            if pedido_date in dates:  # if from month
                if pedido_date in dates[:7]:  # If from week
                    if pedido_date in dates[:1]:  # If from today
                        today_itens.append(item_lst)
                        if pedido[6] == "Uber Eats":
                            today_itens_uber.append(item_lst)
                        elif pedido[6] == "Ifood":
                            today_itens_ifood.append(item_lst)
                        else:
                            discount_d += float(pedido[4])  # Add to discount total
                    else:
                        week_itens.append(item_lst)
                        if pedido[6] == "Uber Eats":
                            week_itens_uber.append(item_lst)
                        elif pedido[6] == "Ifood":
                            week_itens_ifood.append(item_lst)
                        else:
                            discount_w += float(pedido[4])  # Add to discount total
                else:
                    month_itens.append(item_lst)
                    if pedido[6] == "Uber Eats":
                        month_itens_uber.append(item_lst)
                    elif pedido[6] == "Ifood":
                        month_itens_ifood.append(item_lst)
                    else:
                        discount_m += float(pedido[4])  # Add to discount total

        # Finish discount calculation
        discount_w += discount_d
        discount_m += discount_w

        for item in today_itens:
            week_itens.append(item)

        for item in today_itens_ifood:
            week_itens_ifood.append(item)

        for item in today_itens_uber:
            week_itens_uber.append(item)

        for item in week_itens:
            month_itens.append(item)

        for item in week_itens_ifood:
            month_itens_ifood.append(item)

        for item in week_itens_uber:
            month_itens_uber.append(item)

        today_count_uber = len(today_itens_uber)
        today_count_ifood = len(today_itens_ifood)
        today_count = len(today_itens) - today_count_ifood - today_count_uber
        week_count_uber = len(week_itens_uber)
        week_count_ifood = len(week_itens_ifood)
        week_count = len(week_itens) - week_count_uber - week_count_ifood
        month_count_uber = len(month_itens_uber)
        month_count_ifood = len(month_itens_ifood)
        month_count = len(month_itens) - month_count_ifood - month_count_uber

        # Set count labels
        l_num_d = str(today_count) + ' Pedido' if today_count == 1 \
            else str(today_count) + ' Pedidos'
        l_num_d_uber = str(today_count_uber) + ' Pedido' if today_count_uber == 1 \
            else str(today_count_uber) + ' Pedidos'
        l_num_d_ifood = str(today_count_ifood) + ' Pedido' if today_count_ifood == 1 \
            else str(today_count_ifood) + ' Pedidos'
        l_num_w = str(week_count) + ' Pedido' if week_count == 1 \
            else str(week_count) + ' Pedidos'
        l_num_w_uber = str(week_count_uber) + ' Pedido' if week_count_uber == 1 \
            else str(week_count_uber) + ' Pedido'
        l_num_w_ifood = str(week_count_ifood) + ' Pedido' if week_count_ifood == 1 \
            else str(week_count_ifood) + ' Pedidos'
        l_num_m = str(month_count) + ' Pedido' if month_count == 1 \
            else str(month_count) + ' Pedidos'
        l_num_m_uber = str(month_count_uber) + ' Pedido' if month_count_uber == 1 \
            else str(month_count_uber) + ' Pedidos'
        l_num_m_ifood = str(month_count_ifood) + ' Pedido' if month_count_ifood == 1 \
            else str(month_count_ifood) + ' Pedidos'
        self.l_num_d.setText(l_num_d)
        self.l_num_d_uber.setText(l_num_d_uber)
        self.l_num_d_ifood.setText(l_num_d_ifood)
        self.l_num_w.setText(l_num_w)
        self.l_num_w_uber.setText(l_num_w_uber)
        self.l_num_w_ifood.setText(l_num_w_ifood)
        self.l_num_m.setText(l_num_m)
        self.l_num_m_uber.setText(l_num_m_uber)
        self.l_num_m_ifood.setText(l_num_m_ifood)

        # Count items
        today_itens_count = []
        today_itens_count_uber = []
        today_itens_count_ifood = []
        week_itens_count = []
        week_itens_count_uber = []
        week_itens_count_ifood = []
        month_itens_count = []
        month_itens_count_uber = []
        month_itens_count_ifood = []
        cardapio = get_cardapio_nome(database_file)
        item_final_lst_d = []
        item_final_lst_d_uber = []
        item_final_lst_d_ifood = []
        item_final_lst_w = []
        item_final_lst_w_uber = []
        item_final_lst_w_ifood = []
        item_final_lst_m = []
        item_final_lst_m_uber = []
        item_final_lst_m_ifood = []

        for pedido in today_itens:
            for item in pedido:
                today_itens_count.append(item)

        for pedido in today_itens_uber:
            for item in pedido:
                today_itens_count_uber.append(item)

        for pedido in today_itens_ifood:
            for item in pedido:
                today_itens_count_ifood.append(item)

        for pedido in week_itens:
            for item in pedido:
                week_itens_count.append(item)

        for pedido in week_itens_uber:
            for item in pedido:
                week_itens_count_uber.append(item)

        for pedido in week_itens_ifood:
            for item in pedido:
                week_itens_count_ifood.append(item)

        for pedido in month_itens:
            for item in pedido:
                month_itens_count.append(item)

        for pedido in month_itens_uber:
            for item in pedido:
                month_itens_count_uber.append(item)

        for pedido in month_itens_ifood:
            for item in pedido:
                month_itens_count_ifood.append(item)

        for item in cardapio:
            item_final_lst_d.append([item, today_itens_count.count(item)])
            item_final_lst_d_uber.append([item, today_itens_count_uber.count(item)])
            item_final_lst_d_ifood.append([item, today_itens_count_ifood.count(item)])
            item_final_lst_w.append([item, week_itens_count.count(item)])
            item_final_lst_w_uber.append([item, week_itens_count_uber.count(item)])
            item_final_lst_w_ifood.append([item, week_itens_count_ifood.count(item)])
            item_final_lst_m.append([item, month_itens_count.count(item)])
            item_final_lst_m_uber.append([item, month_itens_count_uber.count(item)])
            item_final_lst_m_ifood.append([item, month_itens_count_ifood.count(item)])

        # Add lists to table widgets
        self.tb_vendidos_d.setRowCount(0)

        for item in item_final_lst_d:
            if item[1] > 0:
                row_position = int(self.tb_vendidos_d.rowCount())
                self.tb_vendidos_d.insertRow(row_position)
                for cont, c1 in enumerate(item):
                    self.tb_vendidos_d.setItem(row_position, cont, QTableWidgetItem(str(c1)))

        for item in item_final_lst_w:
            if item[1] > 0:
                row_position = int(self.tb_vendidos_w.rowCount())
                self.tb_vendidos_w.insertRow(row_position)
                for cont, c1 in enumerate(item):
                    self.tb_vendidos_w.setItem(row_position, cont, QTableWidgetItem(str(c1)))

        for item in item_final_lst_m:
            if item[1] > 0:
                row_position = int(self.tb_vendidos_m.rowCount())
                self.tb_vendidos_m.insertRow(row_position)
                for cont, c1 in enumerate(item):
                    self.tb_vendidos_m.setItem(row_position, cont, QTableWidgetItem(str(c1)))

        # Calculate price
        for item in item_final_lst_d:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_d += item_price * item[1]

        for item in item_final_lst_w:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_w += item_price * item[1]

        for item in item_final_lst_m:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_m += item_price * item[1]

        for item in item_final_lst_d_uber:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_d_uber += item_price * item[1]

        for item in item_final_lst_w_uber:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_w_uber += item_price * item[1]

        for item in item_final_lst_m_uber:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_m_uber += item_price * item[1]

        for item in item_final_lst_d_ifood:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_d_ifood += item_price * item[1]

        for item in item_final_lst_w_ifood:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_w_ifood += item_price * item[1]

        for item in item_final_lst_m_ifood:
            item_price = get_preco_id(database_file, get_cardapioid(database_file, item[0]))
            valor_total_m_ifood += item_price * item[1]

        # Get actual price of app sales
        renda_data = get_renda_data(database_file)
        renda_d_ifood, renda_w_ifood, renda_m_ifood = 0, 0, 0
        renda_d_uber, renda_w_uber, renda_m_uber = 0, 0, 0

        for renda in renda_data:
            renda_date = renda[1][:10]
            renda_tipo = renda[2]
            renda_valor = renda[3]

            if renda_date in dates:
                if renda_date in dates[:7]:
                    if renda_date in dates[:1]:
                        if renda_tipo == "Ifood":
                            renda_d_ifood += float(renda_valor)
                        elif renda_tipo == "Uber Eats":
                            renda_d_uber += float(renda_valor)
                    else:
                        if renda_tipo == "Ifood":
                            renda_w_ifood += float(renda_valor)
                        elif renda_tipo == "Uber Eats":
                            renda_w_uber += float(renda_valor)
                else:
                    if renda_tipo == "Ifood":
                        renda_m_ifood += float(renda_valor)
                    elif renda_tipo == "Uber Eats":
                        renda_m_uber += float(renda_valor)

        renda_w_uber += renda_d_uber
        renda_m_uber += renda_w_uber
        renda_w_ifood += renda_d_ifood
        renda_m_ifood += renda_w_ifood

        # Apply discounts
        valor_total_d -= discount_d + valor_total_d_uber + valor_total_d_ifood
        valor_total_w -= discount_w + valor_total_w_uber + valor_total_w_ifood
        valor_total_m -= discount_m + valor_total_m_uber + valor_total_m_ifood

        # Set labels for normal income
        self.l_renda_d.setText(f'R$ {valor_total_d:.2f}')
        self.l_renda_w.setText(f'R$ {valor_total_w:.2f}')
        self.l_renda_m.setText(f'R$ {valor_total_m:.2f}')

        # Set labels for Ifood income
        self.l_renda_d_ifood.setText(f'R$ {renda_d_ifood:.2f}')
        self.l_renda_w_ifood.setText(f'R$ {renda_w_ifood:.2f}')
        self.l_renda_m_ifood.setText(f'R$ {renda_m_ifood:.2f}')

        # Set labels for Uber Eats income
        self.l_renda_d_uber.setText(f'R$ {renda_d_uber:.2f}')
        self.l_renda_w_uber.setText(f'R$ {renda_w_uber:.2f}')
        self.l_renda_m_uber.setText(f'R$ {renda_m_uber:.2f}')

    def add_latest_pedido(self):

        pedidoid = get_latest_pedidoid(database_file)
        pedido = get_pedido_data(database_file, pedidoid)

        client_data = get_cliente_data(database_file, pedido[0])

        entrega = 3 if pedido[4] == '1' else 0

        desconto_num = float(pedido[1])
        desconto = f'R$ {desconto_num:.2f}'

        # Tipo
        tipo = pedido[2]
        appid = return_if_none(pedido[3])
        if appid is None:
            appid = '(Sem AppID)'

        # Get client name
        nome = return_if_none(get_cliente_nome_clienteid(database_file, pedido[0]))
        if nome is None:
            nome = '(Sem Nome)'
        else:
            nome = nome[0] + ' ' + nome[1]

        # Get cliente tel
        tel = client_data[0][2]

        # Address
        endereco = client_data[1]
        cep = endereco[0]
        rua = endereco[1]
        num = endereco[2]
        bairro = endereco[3]
        cidade = endereco[4]

        # Get order items
        items = get_itens_pedido(database_file, pedidoid)

        valor_total = 0
        item_lst = []

        for item in items:
            cardapioid = item[1]
            obs = item[3]
            nome_item = get_cardapio_nome_id(database_file, cardapioid)
            preco = get_preco_id(database_file, cardapioid)
            quant = item[2]
            valor_total += preco * quant
            item_lst.append([nome_item, obs, quant])

        valor_final = valor_total - desconto_num + entrega

        valor_final = f'R$ {valor_final:.2f}'

        info = {
            "Nome": nome,
            "Tipo": tipo,
            "AppID": appid,
            "Desconto": desconto,
            "ValorFinal": valor_final,
            "Telefone": tel,
            "CEP": cep,
            "Rua": rua,
            "Numero": num,
            "Bairro": bairro,
            "Cidade": cidade,
            "PedidoID": pedidoid,
            "Itens": item_lst
        }

        widget = DynamicWidget(info)
        self.scrollAreaWidgetContents.layout().addWidget(widget)

    def add_all_current_pedidos(self):

        pedidos = get_pedido_data(database_file)

        for pedido in pedidos:
            status = pedido[2]
            if status == 'Em Andamento':

                pedidoid = pedido[1]
                clienteid = pedido[0]
                client_data = get_cliente_data(database_file, clienteid)

                entrega = 3 if pedido[8] == '1' else 0

                desconto_num = float(pedido[4])
                desconto = f'R$ {desconto_num:.2f}'

                # Tipo
                tipo = pedido[6]
                appid = return_if_none(pedido[7])
                if appid is None:
                    appid = '(Sem AppID)'

                # Get client name
                nome = return_if_none(get_cliente_nome_clienteid(database_file, clienteid))
                if nome is None:
                    nome = '(Sem Nome)'
                else:
                    nome = nome[0] + ' ' + nome[1]

                # Get cliente tel
                tel = client_data[0][2]

                # Address
                endereco = client_data[1]
                cep = endereco[0]
                rua = endereco[1]
                num = endereco[2]
                bairro = endereco[3]
                cidade = endereco[4]

                # Get order items
                items = get_itens_pedido(database_file, pedidoid)

                valor_total = 0
                item_lst = []

                for item in items:
                    cardapioid = item[1]
                    obs = item[3]
                    nome_item = get_cardapio_nome_id(database_file, cardapioid)
                    preco = get_preco_id(database_file, cardapioid)
                    quant = item[2]
                    valor_total += preco * quant
                    item_lst.append([nome_item, obs, quant])

                valor_final = valor_total - desconto_num + entrega

                valor_final = f'R$ {valor_final:.2f}'

                info = {
                    "Nome": nome,
                    "Tipo": tipo,
                    "AppID": appid,
                    "Desconto": desconto,
                    "ValorFinal": valor_final,
                    "Telefone": tel,
                    "CEP": cep,
                    "Rua": rua,
                    "Numero": num,
                    "Bairro": bairro,
                    "Cidade": cidade,
                    "PedidoID": pedidoid,
                    "Itens": item_lst
                }

                widget = DynamicWidget(info)
                self.scrollAreaWidgetContents.layout().addWidget(widget)

    def update_pedido_tree(self):
        pedidos = get_pedido_data(database_file)

        tree_model = QStandardItemModel()
        root_node = tree_model.invisibleRootItem()

        em_andamento = StandardItem('Pedidos Em Andamento')
        finalizado = StandardItem('Pedidos Concluídos')

        for pedido in pedidos:
            pedidoid = pedido[1]

            entrega = 3 if pedido[8] == '1' else 0

            # Get client name
            clienteid = pedido[0]
            nome = get_cliente_nome_clienteid(database_file, clienteid)
            nome = return_if_none(nome)
            if nome == ('', ''):
                nome = 'Sem Nome'
            else:
                nome = nome[0] + ' ' + nome[1]

            # Get order items
            items = get_itens_pedido(database_file, pedidoid)

            # Define main categories
            pessoa = StandardItem(nome)
            itens = StandardItem('Itens')
            preco = StandardItem('Preço')
            tipo = StandardItem('Tipo')
            origem = StandardItem('Origem')

            valor_total = 0

            for item in items:
                # Variables
                item_name = get_cardapio_nome_id(database_file, item[1])
                item_price = get_preco_id(database_file, item[1])
                item_quant = item[2]
                valor_total += item_price * item_quant

                # Itens
                item_name_item = StandardItem(item_name)
                obs_value = return_if_none(item[3])
                if obs_value is None:
                    obs_value = '(Sem Observação)'

                quant_obj = StandardItem('Quantidade')
                quant = StandardItem(item[2], editable=True)
                quant_obj.appendRow(quant)
                obs_obj = StandardItem('Observação')
                obs = StandardItem(obs_value, editable=True)
                obs_obj.appendRow(obs)
                item_name_item.appendRow(quant_obj)
                item_name_item.appendRow(obs_obj)
                itens.appendRow(item_name_item)

            # Preço
            desconto_valor = float(pedido[4])
            valor_final = float(valor_total) - desconto_valor + entrega
            desconto_valor = f"R$ {desconto_valor:.2f}"
            desconto_motivo = return_if_none(pedido[5])
            if desconto_motivo is None:
                desconto_motivo = 'Sem Motivo'

            if entrega == 3:
                entrega_text = "Sim"
            else:
                entrega_text = "Não"

            entrega_obj_0 = StandardItem('Entrega')
            entrega_obj_1 = StandardItem(entrega_text)
            entrega_obj_0.appendRow(entrega_obj_1)
            preco.appendRow(entrega_obj_0)

            desconto = StandardItem('Desconto')
            desconto_valor_obj = StandardItem(desconto_valor, editable=True)
            desconto.appendRow(desconto_valor_obj)
            desconto_motivo_obj = StandardItem(desconto_motivo, editable=True)
            desconto.appendRow(desconto_motivo_obj)
            preco.appendRow(desconto)
            preco_final_obj = StandardItem('Preço Final')
            preco_final_obj_value = StandardItem(f'R${valor_final:.2f}')
            preco_final_obj.appendRow(preco_final_obj_value)
            preco.appendRow(preco_final_obj)

            # Tipo
            tipo_value = pedido[6]
            appid_value = return_if_none(pedido[7])
            if appid_value is None:
                appid_value = '(Sem AppID)'
            tipo_obj = StandardItem(tipo_value)
            appid_obj = StandardItem(appid_value)
            appid_title = StandardItem('AppID')
            appid_title.appendRow(appid_obj)
            tipo.appendRow(tipo_obj)
            tipo.appendRow(appid_title)

            # Origem
            origem_value = return_if_none(pedido[3])
            if origem_value is None:
                origem_value = '(Sem Origem)'
            origem_obj = StandardItem(origem_value, editable=True)
            origem.appendRow(origem_obj)

            pessoa.appendRow(itens)
            pessoa.appendRow(preco)
            pessoa.appendRow(tipo)
            pessoa.appendRow(origem)

            if pedido[2] == 'Em Andamento':
                em_andamento.appendRow(pessoa)
            elif pedido[2] == 'Concluído':
                finalizado.appendRow(pessoa)
            else:
                raise Exception("what???????????????????????\n", pessoa, "what???????????????????????\n")

        root_node.appendRow(em_andamento)
        root_node.appendRow(finalizado)

        self.treeView.setModel(tree_model)

    def update_cliente(self):
        row_i = self.tableWidget.currentRow()
        clienteid = int(get_cliente_id(database_file, row_i))

        primeironome = self.tableWidget.item(row_i, 0)
        ultimonome = self.tableWidget.item(row_i, 1)
        telefone = self.tableWidget.item(row_i, 2)

        cliente = {"PrimeiroNome": primeironome.text(),
                   "UltimoNome": ultimonome.text(),
                   "Telefone": telefone.text(), "ClienteID": clienteid}

        endereco = {"CEP": self.tableWidget.item(row_i, 3).text(), "Rua": self.tableWidget.item(row_i, 4).text(),
                    "Numero": self.tableWidget.item(row_i, 5).text(), "Bairro": self.tableWidget.item(row_i, 6).text(),
                    "Cidade": self.tableWidget.item(row_i, 7).text(), "ClienteID": clienteid}

        update_row(database_file, 'endereco', clienteid, endereco)
        update_row(database_file, 'cliente', clienteid, cliente)
        self.update_table()
        self.update_field_clientes()
        self.update_pedido_tree()

    def remove_cliente(self):
        row_i = self.tableWidget.currentRow()
        clienteid = str(get_cliente_id(database_file, row_i))
        remove_cliente_id(database_file, clienteid)
        self.update_table()
        self.update_field_clientes()

    def update_table(self):
        # self.tableWidget.clear()
        self.tableWidget.setRowCount(0)
        table = get_cliente_data(database_file)
        # self.tableWidget.setColumnCount(len(table))
        for n in table:
            row_position = int(self.tableWidget.rowCount())
            self.tableWidget.insertRow(row_position)
            for cont, c1 in enumerate(n):
                if c1 is None:
                    c1 = ''
                self.tableWidget.setItem(row_position, cont, QTableWidgetItem(str(c1)))

    def add_to_list(self):
        item = self.comboBox_2.currentText()
        if item not in "Escolha um item do cardápio":
            obs = self.lineEdit_13.text().strip()
            self.lineEdit_13.setText('')

            for x in range(self.spinBox.value()):
                self.listWidget.addItem(item)
                self.listWidget_2.addItem(obs)

            self.spinBox.setValue(1)

    def clear_list(self):
        self.listWidget.clear()
        self.listWidget_2.clear()

    def remove_from_list(self):
        index = int(self.listWidget.currentIndex().row())
        self.listWidget.takeItem(index)
        self.listWidget_2.takeItem(index)

    def add_order(self):
        if self.comboBox.currentText() not in ['Nenhum cliente registrado', 'Escolha um cliente']\
                and self.listWidget.count() > 0:

            # Get name
            if self.comboBox.currentText() == "Sem Nome":
                nome = ' '
            else:
                nome = self.comboBox.currentText()
            pedido = {"Tipo": self.current_radio,
                      "ClienteID": get_client_id(database_file, nome),
                      "Origem": self.lineEdit_6.text().strip(),
                      "AppID": self.lineEdit_11.text().strip(),
                      "Desconto": self.lineEdit_7.text().replace(',', '.'),
                      "DescontoMotivo": self.lineEdit_12.text().strip(),
                      "Entrega": str(1 if self.checkBox.isChecked() else 0)}
            add_row(database_file, 'pedido', pedido)

            pedidoid = get_latest_pedidoid(database_file)
            items = []
            num_list = self.listWidget.count()

            for n in range(num_list):
                item_0 = self.listWidget.takeItem(0).text()
                obs_0 = return_if_none(self.listWidget_2.takeItem(0).text())

                found = False

                # Acho que demorei uma hora para desenvolver essas 7 linhas de código aqui em baixo, pelamor.
                for c, d in enumerate(items):
                    if d[0] == item_0 and d[1] == obs_0:
                        found = True
                        items[c][2] += 1
                        break

                if not found:
                    items.append([item_0, obs_0, 1])

            for n in items:
                cardapioid = get_cardapioid(database_file, n[0])
                obs = n[1]
                quant = int(n[2])

                add_row(database_file, 'item', {"PedidoID": pedidoid, "CardapioID": cardapioid,
                                                "Quantidade": quant, "Observacoes": obs})

            self.lineEdit_7.setText('')
            self.lineEdit_12.setText('')
            self.lineEdit_6.setText('')
            self.lineEdit_11.setText('')
            self.lineEdit_3.setText('')
            self.checkBox.setChecked(False)
            self.radioButton.setChecked(True)
            self.update_pedido_tree()
            self.add_latest_pedido()
        else:
            print("Informações faltando!")

    def radio_clicked(self):
        radio_btn = self.sender()
        if radio_btn.isChecked():
            self.current_radio = radio_btn.text()
            if self.current_radio == 'Uber Eats' or self.current_radio == 'Ifood':
                self.lineEdit_11.setReadOnly(False)
            elif self.current_radio == 'Comum':
                self.lineEdit_11.clear()
                self.lineEdit_11.setReadOnly(True)

    def get_cep(self):
        cep = self.lineEdit_2.text()
        cep_info = get_info_cep(cep)
        if cep_info:
            self.lineEdit_2.setPlaceholderText('')
            self.lineEdit_8.setText(cep_info["logradouro"])
            self.lineEdit_9.setText(cep_info["bairro"])
            self.lineEdit_10.setText(cep_info["cidade"]["nome"])
        else:
            self.lineEdit_2.setText('')
            self.lineEdit_2.setPlaceholderText('CEP não existe.')

    def add_client(self):
        cliente = {
            "PrimeiroNome": self.lineEdit_4.text().strip().title(),
            "UltimoNome": self.lineEdit_5.text().strip().title(),
            "Telefone": self.lineEdit.text().strip()
        }
        add_row(database_file, 'cliente', cliente)

        endereco = {
            "ClienteID": get_latest_clienteid(database_file),
            "CEP": self.lineEdit_2.text(),
            "Numero": self.lineEdit_3.text().strip(),
            "Rua": self.lineEdit_8.text().strip(),
            "Bairro": self.lineEdit_9.text().strip(),
            "Cidade": self.lineEdit_10.text().strip()
        }

        add_row(database_file, 'endereco', endereco)

        self.lineEdit_3.setText('')
        self.lineEdit_4.setText('')
        self.lineEdit_5.setText('')
        self.lineEdit_2.setText('')
        self.lineEdit_8.setText('')
        self.lineEdit_9.setText('')
        self.lineEdit_10.setText('')
        self.lineEdit.setText('')

        self.update_table()
        self.update_field_clientes()

    def update_field_clientes(self):
        nome_clientes = get_cliente_nome(database_file)
        if not nome_clientes:
            self.comboBox.clear()
            self.comboBox.addItem('Nenhum cliente registrado')
        else:
            self.comboBox.clear()
            if len(nome_clientes) > 1:
                self.comboBox.addItem('Escolha um cliente')
            for nome in nome_clientes:
                if nome == ' ':
                    nome = 'Sem Nome'
                self.comboBox.addItem(nome)
            # self.comboBox.addItems(nome_clientes)

    def update_field_cardapio(self):
        cardapio = get_cardapio_nome(database_file)
        self.comboBox_2.addItem('Escolha um item do cardápio')
        self.comboBox_2.addItems(cardapio)


# Handle SIGINT (CTRL-C on Linux)
signal.signal(signal.SIGINT, signal.SIG_DFL)

path.dirname(path.abspath(__file__))

# Database filename
database_file = 'database.db'

# Start application
app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec()
