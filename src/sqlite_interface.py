# SQLite interface for Caldinho Bocaina Software
# Author: Lucas Santos
# Start Date: 05/07/2021

# LICENSE
# This file is part of caldinho-bocaina-v2.
#
#    caldinho-bocaina-v2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    caldinho-bocaina-v2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with caldinho-bocaina-v2.  If not, see <https://www.gnu.org/licenses/>.

def create_database(filename):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        print('Database already exists!')
    else:
        print('Creating database...')
        con = sqlite3.connect(filename)
        cur = con.cursor()

        cur.executescript("""
        CREATE TABLE IF NOT EXISTS "cardapio" (
        "CardapioID"	INTEGER NOT NULL UNIQUE,
        "Nome"	TEXT NOT NULL UNIQUE,
        "Tipo"	TEXT NOT NULL,
        "Preco"	REAL NOT NULL,
        "QuantidadeGramas"	INTEGER,
        "QuantidadeMililitros"	INTEGER,
        PRIMARY KEY("CardapioID" AUTOINCREMENT)
        );
        CREATE TABLE IF NOT EXISTS "cliente" (
        "ClienteID"	INTEGER NOT NULL UNIQUE,
        "PrimeiroNome"	TEXT,
        "UltimoNome"	TEXT,
        "Telefone"	TEXT,
        PRIMARY KEY("ClienteID")
        );
        CREATE TABLE IF NOT EXISTS "endereco" (
        "ClienteID"	INTEGER NOT NULL UNIQUE,
        "CEP"	TEXT,
        "Numero"	TEXT,
        "Rua"	TEXT,
        "Bairro"	TEXT,
        "Cidade"	TEXT,
        FOREIGN KEY("ClienteID") REFERENCES "cliente"("ClienteID")
        );
        CREATE TABLE IF NOT EXISTS "item" (
        "PedidoID"	INTEGER NOT NULL,
        "CardapioID"	INTEGER NOT NULL,
        "Quantidade"	INTEGER NOT NULL,
        "Observacoes"	TEXT,
        FOREIGN KEY("CardapioID") REFERENCES "cardapio"("CardapioID"),
        FOREIGN KEY("PedidoID") REFERENCES "pedido"("PedidoID")
        );
        CREATE TABLE IF NOT EXISTS "pedido" (
        "PedidoID"	INTEGER NOT NULL UNIQUE,
        "ClienteID"	INTEGER NOT NULL,
        "Data"	TEXT NOT NULL,
        "Status"	TEXT NOT NULL,
        "Origem"	TEXT,
        "Desconto"	TEXT DEFAULT 0,
        "DescontoMotivo"	TEXT,
        "Tipo"	TEXT NOT NULL,
        "AppID"	TEXT,
        "Entrega"	TEXT NOT NULL DEFAULT 0,
        FOREIGN KEY("ClienteID") REFERENCES "cliente"("ClienteID"),
        PRIMARY KEY("PedidoID" AUTOINCREMENT)
        );
        CREATE TABLE IF NOT EXISTS "renda_apps" (
        "RendaID"	INTEGER NOT NULL UNIQUE,
        "Data"	TEXT NOT NULL,
        "Tipo"	TEXT NOT NULL,
        "Valor"	TEXT NOT NULL,
        PRIMARY KEY("RendaID" AUTOINCREMENT));
        """)

        con.commit()
        con.close()

        print('Database created and saved to disk!')


def add_row(filename, table, values):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        itens = []

        if table == 'cliente':
            itens.append(values["PrimeiroNome"])
            itens.append(values["UltimoNome"])
            itens.append(values["Telefone"])
            q = 'insert into cliente(PrimeiroNome, UltimoNome, Telefone) values (?, ?, ?)'
            cur.execute(q, itens)
        elif table == 'pedido':
            itens.append(values["ClienteID"])

            itens.append(return_if_none(values["Origem"]))

            if values["Desconto"] == '':
                itens.append(0)
            else:
                itens.append(values["Desconto"])

            itens.append(return_if_none(values["DescontoMotivo"]))
            itens.append(return_if_none(values["Tipo"]))
            itens.append(return_if_none(values["AppID"]))
            itens.append(values["Entrega"])

            q = 'insert into pedido(Data, Status, ClienteID, Origem, Desconto, DescontoMotivo, Tipo, AppID, Entrega) ' \
                'values (datetime("now", "localtime"), "Em Andamento", ?, ?, ?, ?, ?, ?, ?)'
            cur.execute(q, itens)
        elif table == 'item':
            itens.append(values["PedidoID"])
            itens.append(values["CardapioID"])
            itens.append(values["Quantidade"])
            itens.append(return_if_none(values["Observacoes"]))
            q = 'insert into item(PedidoID, CardapioID, Quantidade, Observacoes) values (?, ?, ?, ?)'
            cur.execute(q, itens)
        elif table == 'cardapio':
            itens.append(values["Nome"])
            itens.append(values["Tipo"])
            itens.append(values["Preco"])
            itens.append(return_if_none(["QuantidadeGramas"]))
            itens.append(return_if_none(["QuantidadeMililitros"]))
            q = 'insert into cardapio(Nome, Tipo, Preco, QuantidadeGramas, QuantidadeMililitros)' \
                'values (?, ?, ?, ?, ?)'
            cur.execute(q, itens)
        elif table == 'endereco':
            itens.append(return_if_none(values["ClienteID"]))
            itens.append(return_if_none(values["CEP"]))
            itens.append(return_if_none(values["Numero"]))
            itens.append(return_if_none(values["Rua"]))
            itens.append(return_if_none(values["Bairro"]))
            itens.append(return_if_none(values["Cidade"]))
            q = 'insert into endereco(ClienteID, CEP, Numero, Rua, Bairro, Cidade)' \
                'values (?, ?, ?, ?, ?, ?)'
            cur.execute(q, itens)
        elif table == 'renda_apps':
            itens.append(values["Tipo"])
            itens.append(str(values["Valor"]))
            q = 'insert into renda_apps(Data, Tipo, Valor) values (datetime("now", "localtime"), ?, ?)'
            cur.execute(q, itens)

        con.commit()
        con.close()

    else:
        print(f'ERROR: {filename} does not exist!')


def delete_row(filename, table, row_id):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        if table == 'cliente':
            q = 'DELETE FROM endereco WHERE ClienteID=(?)'
            cur.execute(q, (row_id["ClienteID"],))
            q = 'DELETE FROM cliente WHERE ClienteID=(?)'
            cur.execute(q, (row_id["ClienteID"],))

        elif table == 'pedido':
            q = 'DELETE FROM item WHERE PedidoID=(?)'
            cur.execute(q, (row_id["PedidoID"],))
            q = 'DELETE FROM pedido WHERE PedidoID=(?)'
            cur.execute(q, (row_id["PedidoID"],))
        else:
            print(f"ERROR: Table '{table}' does not exist!")
        con.commit()
        con.close()

    else:
        print(f"ERROR: {filename} does not exist!")


def update_row(filename, table, row_id, update):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()
        itens = []

        if table == 'cliente':
            itens.append(update["PrimeiroNome"])
            itens.append(update["UltimoNome"])
            itens.append(update["Telefone"])
            itens.append(update["ClienteID"])
            q = 'UPDATE cliente SET PrimeiroNome = (?), UltimoNome = (?), Telefone = (?) WHERE ClienteID=(?)'
            cur.execute(q, itens)

        elif table == 'endereco':
            itens.append(update["CEP"])
            itens.append(update["Numero"])
            itens.append(update["Rua"])
            itens.append(update["Bairro"])
            itens.append(update["Cidade"])
            itens.append(update["ClienteID"])
            q = 'UPDATE endereco SET CEP = (?), Numero = (?), Rua = (?), Bairro = (?), Cidade = (?) ' \
                'WHERE ClienteID=(?)'
            cur.execute(q, itens)

        elif table == 'pedido':
            itens.append(update["ClienteID"])
            itens.append(update["Data"])
            itens.append(update["Status"])
            itens.append(update["Origem"])
            itens.append(update["Desconto"])
            itens.append(update["DescontoMotivo"])
            itens.append(update["Tipo"])
            itens.append(update["AppID"])
            itens.append(update["Entrega"])
            itens.append(row_id["PedidoID"])
            q = 'UPDATE pedido SET ClienteID = (?), Data = (?), Status = (?), Origem = (?), Desconto = (?), ' \
                'DescontoMotivo = (?), Tipo = (?), AppID = (?), Entrega = (?) WHERE PedidoID=(?)'
            cur.execute(q, itens)

        elif table == 'cardapio':
            itens.append(update["Nome"])
            itens.append(update["Tipo"])
            itens.append(update["Preco"])
            itens.append(update["QuantidadeGramas"])
            itens.append(update["QuantidadeMililitros"])
            itens.append(row_id["CardapioID"])
            q = 'UPDATE pedido SET Nome = (?), Tipo = (?), Preco = (?), QuantidadeGramas = (?), ' \
                'QuantidadeMililitros = (?) WHERE CardapioID=(?)'
            cur.execute(q, itens)

        elif table == 'item':
            itens.append(update["Quantidade"])
            itens.append(update["Observacoes"])
            itens.append(row_id["PedidoID"])
            itens.append(row_id["CardapioID"])
            q = 'UPDATE item SET Quantidade = (?), Observacoes = (?) WHERE PedidoID = (?) AND CardapioID = (?)'
            cur.execute(q, itens)

        else:
            print(f"ERROR: Table '{table}' does not exist!")

        con.commit()
        con.close()
    else:
        print(f"ERROR: {filename} does not exist!")


def get_cliente_nome(filename):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()
        presult = []
        q = 'SELECT PrimeiroNome, UltimoNome FROM cliente ORDER BY ClienteID DESC'
        cur.execute(q)
        result = cur.fetchall()
        con.close()

        for n in result:
            presult.append(n[0] + ' ' + n[1])

        return presult
    else:
        print(f"ERROR: {filename} does not exist!")


def get_cardapio_nome(filename):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()
        presult = []
        q = 'SELECT Nome FROM cardapio ORDER BY CardapioID ASC'
        cur.execute(q)
        result = cur.fetchall()
        con.close()
        for n in result:
            presult.append(n[0])
        return presult
    else:
        print(f"ERROR: {filename} does not exist!")


def get_client_id(filename, nome):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = 'SELECT PrimeiroNome, UltimoNome, ClienteID FROM cliente ORDER BY ClienteID ASC'
        cur.execute(q)

        clienteid = None

        result = cur.fetchall()[0:]
        for n in result:
            if n[0] + ' ' + n[1] == nome:
                clienteid = n[2]

        con.close()

        if clienteid is not None:
            return clienteid
        else:
            raise Exception(f"ClienteID is not defined, please check if '{nome}' exists on the database!")

    else:
        print(f"ERROR: {filename} does not exist!")


def get_cardapioid(filename, nome):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = 'SELECT CardapioID FROM cardapio WHERE Nome = ?'
        cur.execute(q, (nome,))
        result = cur.fetchone()[0]
        con.close()
        return result
    else:
        print(f"ERROR: {filename} does not exist!")


def return_if_none(value):
    if value == '':
        return None
    else:
        return value


def get_latest_pedidoid(filename):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()
        q = "SELECT PedidoID FROM pedido ORDER BY PedidoID DESC LIMIT 1"

        cur.execute(q)
        result = cur.fetchone()[0]
        con.close()
        return result

    else:
        print(f"ERROR: {filename} does not exist!")


def get_latest_clienteid(filename):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()
        q = "SELECT ClienteID FROM cliente ORDER BY ClienteID DESC LIMIT 1"

        cur.execute(q)
        result = cur.fetchone()[0]
        con.close()
        return result

    else:
        print(f"ERROR: {filename} does not exist!")


def get_cliente_data(filename, clienteid=None):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        if clienteid is None:
            q = "SELECT PrimeiroNome, UltimoNome, Telefone, endereco.CEP, " \
                "endereco.Rua, endereco.Numero, endereco.Bairro," \
                "endereco.Cidade FROM cliente INNER JOIN endereco ON cliente.ClienteID = endereco.ClienteID " \
                "ORDER BY cliente.ClienteID DESC"

            cur.execute(q)
            result = cur.fetchall()
        else:
            clienteid = str(clienteid)
            q = "SELECT PrimeiroNome, UltimoNome, Telefone FROM cliente WHERE ClienteID = ?"
            cur.execute(q, (clienteid,))
            result = [cur.fetchone()]
            q = "SELECT CEP, Rua, Numero, Bairro, Cidade FROM endereco WHERE ClienteID = ?"
            cur.execute(q, (clienteid,))
            result.append(cur.fetchone())

        cur.close()
        return result

    else:
        print(f'ERROR: {filename} does not exist!')


def get_cliente_id(filename, index):
    """
    Get ClienteID with a index of a descending ClienteID list
    :param filename: (str) filename of the database
    :param index: (int) index of value
    :return: (str) returns ClienteID
    """
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = "SELECT ClienteID FROM cliente ORDER BY ClienteID DESC LIMIT 1 OFFSET ?"
        cur.execute(q, (str(index),))
        result = cur.fetchone()[0]
        con.close()
        return result

    else:
        print(f'ERROR: {filename} does not exist!')


def remove_cliente_id(filename, cliente_id):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()
        q = 'DELETE FROM endereco WHERE ClienteID = ?'
        cur.execute(q, (cliente_id,))

        q = 'DELETE FROM cliente WHERE ClienteID = ?'
        cur.execute(q, (cliente_id,))

        con.commit()
        con.close()

    else:
        print(f'ERROR: {filename} does not exist!')


def get_cliente_nome_clienteid(filename, clienteid):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = "SELECT PrimeiroNome, UltimoNome FROM cliente WHERE ClienteID = ?"
        cur.execute(q, (str(clienteid),))
        result = cur.fetchone()
        con.close()
        return result

    else:
        print(f'ERROR: {filename} does not exist!')


def get_pedido_data(filename, pedidoid=None):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        if pedidoid is None:
            q = "SELECT ClienteID, PedidoID, Status, Origem, Desconto, DescontoMotivo, Tipo, AppID, Entrega, Data " \
                "FROM pedido " \
                "ORDER BY PedidoID DESC"
            cur.execute(q)
            result = cur.fetchall()

        else:
            q = "SELECT ClienteID, Desconto, Tipo, AppID, Entrega FROM pedido WHERE PedidoID = ?"
            cur.execute(q, (str(pedidoid),))
            result = cur.fetchone()

        con.close()
        return result

    else:
        print(f'ERROR: {filename} does not exist!')


def get_item_data(filename):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = "SELECT PedidoID, CardapioID, Quantidade, Observacoes " \
            "FROM item " \
            "ORDER BY PedidoID DESC"

        cur.execute(q)
        result = cur.fetchall()
        con.close()
        return result

    else:
        print(f'ERROR: {filename} does not exist!')


def get_renda_data(filename):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = "SELECT RendaID, Data, Tipo, Valor " \
            "FROM renda_apps " \
            "ORDER BY RendaID DESC"

        cur.execute(q)
        result = cur.fetchall()
        con.close()
        return result

    else:
        print(f'ERROR: {filename} does not exist!')


def get_itens_pedido(filename, pedidoid):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = "SELECT PedidoID, CardapioID, Quantidade, Observacoes FROM item WHERE PedidoID = ? ORDER BY CardapioID ASC"

        cur.execute(q, (str(pedidoid),))
        result = cur.fetchall()
        con.close()
        return result

    else:
        print(f'ERROR: {filename} does not exist!')


def get_cardapio_nome_id(filename, cardapioid):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = "SELECT Nome FROM cardapio WHERE CardapioID = ?"

        cur.execute(q, (str(cardapioid),))
        result = cur.fetchone()[0]
        con.close()
        return result

    else:
        print(f'ERROR: {filename} does not exist!')


def get_preco_id(filename, cardapioid):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = "SELECT Preco FROM cardapio WHERE CardapioID = ?"

        cur.execute(q, (str(cardapioid),))
        result = cur.fetchone()[0]
        con.close()
        return result
    else:
        print(f'ERROR: {filename} does not exist!')


def conclude_pedido(filename, pedidoid):
    import sqlite3
    from os import path

    if path.exists(f'./{filename}'):
        con = sqlite3.connect(filename)
        cur = con.cursor()

        q = "UPDATE Pedido SET status = 'Concluído' WHERE PedidoID = ?"

        cur.execute(q, (str(pedidoid),))
        con.commit()
        con.close()
    else:
        print(f'ERROR: {filename} does not exist!')
