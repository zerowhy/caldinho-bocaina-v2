# LICENSE
# This file is part of caldinho-bocaina-v2.
#
#    caldinho-bocaina-v2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    caldinho-bocaina-v2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with caldinho-bocaina-v2.  If not, see <https://www.gnu.org/licenses/>.

def get_info_cep(cep):
    import requests

    # Token API cepaberto.com - Necessário para o funcionamento do software.
    token_file = open('token.txt')
    token = token_file.read()

    url = f"https://www.cepaberto.com/api/v3/cep?cep={cep}"
    headers = {'Authorization': f'Token token={token}'}
    response = requests.get(url, headers=headers)
    result = response.json()

    return result
