# LICENSE
# This file is part of caldinho-bocaina-v2.
#
#    caldinho-bocaina-v2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    caldinho-bocaina-v2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with caldinho-bocaina-v2.  If not, see <https://www.gnu.org/licenses/>.


# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'custom.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1026, 686)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setContentsMargins(0, 0, 0, 20)
        self.verticalLayout.setObjectName("verticalLayout")
        self.main_frame = QtWidgets.QFrame(Form)
        self.main_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.main_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.main_frame.setObjectName("main_frame")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout(self.main_frame)
        self.verticalLayout_9.setContentsMargins(0, 5, 0, 0)
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.l_nome_cliente = QtWidgets.QLabel(self.main_frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_nome_cliente.sizePolicy().hasHeightForWidth())
        self.l_nome_cliente.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(17)
        self.l_nome_cliente.setFont(font)
        self.l_nome_cliente.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.l_nome_cliente.setObjectName("l_nome_cliente")
        self.verticalLayout_9.addWidget(self.l_nome_cliente)
        self.line_2 = QtWidgets.QFrame(self.main_frame)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout_9.addWidget(self.line_2)
        self.w_info = QtWidgets.QWidget(self.main_frame)
        self.w_info.setObjectName("w_info")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.w_info)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.w_pedido_info = QtWidgets.QWidget(self.w_info)
        self.w_pedido_info.setObjectName("w_pedido_info")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.w_pedido_info)
        self.verticalLayout_2.setContentsMargins(0, 0, -1, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.widget = QtWidgets.QWidget(self.w_pedido_info)
        self.widget.setObjectName("widget")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout_4.setContentsMargins(-1, 0, -1, 0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.widget_3 = QtWidgets.QWidget(self.widget)
        self.widget_3.setObjectName("widget_3")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.widget_3)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.widget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.l_tipo = QtWidgets.QLabel(self.widget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_tipo.sizePolicy().hasHeightForWidth())
        self.l_tipo.setSizePolicy(sizePolicy)
        self.l_tipo.setObjectName("l_tipo")
        self.horizontalLayout_2.addWidget(self.l_tipo)
        self.label = QtWidgets.QLabel(self.widget_3)
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.label_5 = QtWidgets.QLabel(self.widget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_2.addWidget(self.label_5)
        self.l_appid = QtWidgets.QLabel(self.widget_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_appid.sizePolicy().hasHeightForWidth())
        self.l_appid.setSizePolicy(sizePolicy)
        self.l_appid.setObjectName("l_appid")
        self.horizontalLayout_2.addWidget(self.l_appid)
        self.verticalLayout_4.addWidget(self.widget_3)
        self.widget_4 = QtWidgets.QWidget(self.widget)
        self.widget_4.setObjectName("widget_4")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.widget_4)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.widget_8 = QtWidgets.QWidget(self.widget_4)
        self.widget_8.setObjectName("widget_8")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.widget_8)
        self.verticalLayout_5.setContentsMargins(0, 0, 2, 0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label_7 = QtWidgets.QLabel(self.widget_8)
        self.label_7.setAlignment(QtCore.Qt.AlignCenter)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_5.addWidget(self.label_7)
        self.lw_itens = QtWidgets.QListWidget(self.widget_8)
        self.lw_itens.setObjectName("lw_itens")
        self.verticalLayout_5.addWidget(self.lw_itens)
        self.horizontalLayout_3.addWidget(self.widget_8)
        self.widget_7 = QtWidgets.QWidget(self.widget_4)
        self.widget_7.setObjectName("widget_7")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.widget_7)
        self.verticalLayout_6.setContentsMargins(2, 0, 0, 0)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.label_8 = QtWidgets.QLabel(self.widget_7)
        self.label_8.setAlignment(QtCore.Qt.AlignCenter)
        self.label_8.setObjectName("label_8")
        self.verticalLayout_6.addWidget(self.label_8)
        self.lw_observacoes = QtWidgets.QListWidget(self.widget_7)
        self.lw_observacoes.setObjectName("lw_observacoes")
        self.verticalLayout_6.addWidget(self.lw_observacoes)
        self.horizontalLayout_3.addWidget(self.widget_7)
        self.verticalLayout_4.addWidget(self.widget_4)
        self.widget_9 = QtWidgets.QWidget(self.widget)
        self.widget_9.setObjectName("widget_9")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.widget_9)
        self.horizontalLayout_6.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.frame = QtWidgets.QFrame(self.widget_9)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.w_desconto = QtWidgets.QWidget(self.frame)
        self.w_desconto.setObjectName("w_desconto")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.w_desconto)
        self.horizontalLayout_5.setContentsMargins(-1, 0, -1, 0)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_4 = QtWidgets.QLabel(self.w_desconto)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_5.addWidget(self.label_4)
        self.l_desconto = QtWidgets.QLabel(self.w_desconto)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_desconto.sizePolicy().hasHeightForWidth())
        self.l_desconto.setSizePolicy(sizePolicy)
        self.l_desconto.setObjectName("l_desconto")
        self.horizontalLayout_5.addWidget(self.l_desconto)
        self.verticalLayout_7.addWidget(self.w_desconto)
        self.w_valor_final = QtWidgets.QWidget(self.frame)
        self.w_valor_final.setObjectName("w_valor_final")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.w_valor_final)
        self.horizontalLayout_4.setContentsMargins(-1, 0, -1, 0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_6 = QtWidgets.QLabel(self.w_valor_final)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.l_valor_final = QtWidgets.QLabel(self.w_valor_final)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_valor_final.sizePolicy().hasHeightForWidth())
        self.l_valor_final.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.l_valor_final.setFont(font)
        self.l_valor_final.setObjectName("l_valor_final")
        self.horizontalLayout_4.addWidget(self.l_valor_final)
        self.verticalLayout_7.addWidget(self.w_valor_final)
        self.horizontalLayout_6.addWidget(self.frame)
        self.b_finalizar = QtWidgets.QPushButton(self.widget_9)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.b_finalizar.sizePolicy().hasHeightForWidth())
        self.b_finalizar.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.b_finalizar.setFont(font)
        self.b_finalizar.setObjectName("b_finalizar")
        self.horizontalLayout_6.addWidget(self.b_finalizar)
        self.verticalLayout_4.addWidget(self.widget_9)
        self.verticalLayout_2.addWidget(self.widget)
        self.horizontalLayout.addWidget(self.w_pedido_info)
        self.line = QtWidgets.QFrame(self.w_info)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout.addWidget(self.line)
        self.w_cliente_info = QtWidgets.QWidget(self.w_info)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.w_cliente_info.sizePolicy().hasHeightForWidth())
        self.w_cliente_info.setSizePolicy(sizePolicy)
        self.w_cliente_info.setObjectName("w_cliente_info")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.w_cliente_info)
        self.verticalLayout_3.setContentsMargins(-1, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_2 = QtWidgets.QLabel(self.w_cliente_info)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label_2.setFont(font)
        self.label_2.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_3.addWidget(self.label_2)
        self.widget_2 = QtWidgets.QWidget(self.w_cliente_info)
        self.widget_2.setObjectName("widget_2")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.widget_2)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.w_tel = QtWidgets.QWidget(self.widget_2)
        self.w_tel.setObjectName("w_tel")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.w_tel)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label_9 = QtWidgets.QLabel(self.w_tel)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_9.setFont(font)
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_7.addWidget(self.label_9)
        self.l_tel = QtWidgets.QLabel(self.w_tel)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.l_tel.setFont(font)
        self.l_tel.setObjectName("l_tel")
        self.horizontalLayout_7.addWidget(self.l_tel)
        self.verticalLayout_8.addWidget(self.w_tel)
        self.widget_10 = QtWidgets.QWidget(self.widget_2)
        self.widget_10.setObjectName("widget_10")
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout(self.widget_10)
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.label_11 = QtWidgets.QLabel(self.widget_10)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_11.sizePolicy().hasHeightForWidth())
        self.label_11.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_11.setFont(font)
        self.label_11.setObjectName("label_11")
        self.horizontalLayout_9.addWidget(self.label_11)
        self.l_cep = QtWidgets.QLabel(self.widget_10)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.l_cep.setFont(font)
        self.l_cep.setObjectName("l_cep")
        self.horizontalLayout_9.addWidget(self.l_cep)
        self.verticalLayout_8.addWidget(self.widget_10)
        self.widget_11 = QtWidgets.QWidget(self.widget_2)
        self.widget_11.setObjectName("widget_11")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout(self.widget_11)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.label_12 = QtWidgets.QLabel(self.widget_11)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_12.sizePolicy().hasHeightForWidth())
        self.label_12.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_12.setFont(font)
        self.label_12.setObjectName("label_12")
        self.horizontalLayout_10.addWidget(self.label_12)
        self.l_rua = QtWidgets.QLabel(self.widget_11)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.l_rua.setFont(font)
        self.l_rua.setObjectName("l_rua")
        self.horizontalLayout_10.addWidget(self.l_rua)
        self.verticalLayout_8.addWidget(self.widget_11)
        self.widget_13 = QtWidgets.QWidget(self.widget_2)
        self.widget_13.setObjectName("widget_13")
        self.horizontalLayout_12 = QtWidgets.QHBoxLayout(self.widget_13)
        self.horizontalLayout_12.setObjectName("horizontalLayout_12")
        self.label_14 = QtWidgets.QLabel(self.widget_13)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_14.sizePolicy().hasHeightForWidth())
        self.label_14.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_14.setFont(font)
        self.label_14.setObjectName("label_14")
        self.horizontalLayout_12.addWidget(self.label_14)
        self.l_num = QtWidgets.QLabel(self.widget_13)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.l_num.setFont(font)
        self.l_num.setObjectName("l_num")
        self.horizontalLayout_12.addWidget(self.l_num)
        self.verticalLayout_8.addWidget(self.widget_13)
        self.widget_12 = QtWidgets.QWidget(self.widget_2)
        self.widget_12.setObjectName("widget_12")
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout(self.widget_12)
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.label_13 = QtWidgets.QLabel(self.widget_12)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_13.sizePolicy().hasHeightForWidth())
        self.label_13.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_13.setFont(font)
        self.label_13.setObjectName("label_13")
        self.horizontalLayout_11.addWidget(self.label_13)
        self.l_bairro = QtWidgets.QLabel(self.widget_12)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.l_bairro.setFont(font)
        self.l_bairro.setObjectName("l_bairro")
        self.horizontalLayout_11.addWidget(self.l_bairro)
        self.verticalLayout_8.addWidget(self.widget_12)
        self.widget_6 = QtWidgets.QWidget(self.widget_2)
        self.widget_6.setObjectName("widget_6")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout(self.widget_6)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.label_10 = QtWidgets.QLabel(self.widget_6)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_10.sizePolicy().hasHeightForWidth())
        self.label_10.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.label_10.setFont(font)
        self.label_10.setObjectName("label_10")
        self.horizontalLayout_8.addWidget(self.label_10)
        self.l_cidade = QtWidgets.QLabel(self.widget_6)
        font = QtGui.QFont()
        font.setPointSize(13)
        self.l_cidade.setFont(font)
        self.l_cidade.setObjectName("l_cidade")
        self.horizontalLayout_8.addWidget(self.l_cidade)
        self.verticalLayout_8.addWidget(self.widget_6)
        self.verticalLayout_3.addWidget(self.widget_2)
        self.horizontalLayout.addWidget(self.w_cliente_info)
        self.verticalLayout_9.addWidget(self.w_info)
        self.verticalLayout.addWidget(self.main_frame)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.l_nome_cliente.setText(_translate("Form", "NomeCliente"))
        self.label_3.setText(_translate("Form", "Tipo:"))
        self.l_tipo.setText(_translate("Form", "<Tipo>"))
        self.label.setText(_translate("Form", "Informações do Pedido"))
        self.label_5.setText(_translate("Form", "AppID:"))
        self.l_appid.setText(_translate("Form", "<AppID>"))
        self.label_7.setText(_translate("Form", "Itens"))
        self.label_8.setText(_translate("Form", "Observações"))
        self.label_4.setText(_translate("Form", "Desconto:"))
        self.l_desconto.setText(_translate("Form", "<Desconto>"))
        self.label_6.setText(_translate("Form", "Valor Final:"))
        self.l_valor_final.setText(_translate("Form", "<ValorFinal>"))
        self.b_finalizar.setText(_translate("Form", "Finalizar Pedido"))
        self.label_2.setText(_translate("Form", "Informações do Cliente"))
        self.label_9.setText(_translate("Form", "Telefone:"))
        self.l_tel.setText(_translate("Form", "<tel>"))
        self.label_11.setText(_translate("Form", "CEP:"))
        self.l_cep.setText(_translate("Form", "<cep>"))
        self.label_12.setText(_translate("Form", "Rua:"))
        self.l_rua.setText(_translate("Form", "<rua>"))
        self.label_14.setText(_translate("Form", "Número:"))
        self.l_num.setText(_translate("Form", "<num>"))
        self.label_13.setText(_translate("Form", "Bairro:"))
        self.l_bairro.setText(_translate("Form", "<bairro>"))
        self.label_10.setText(_translate("Form", "Cidade:"))
        self.l_cidade.setText(_translate("Form", "<cidade>"))
