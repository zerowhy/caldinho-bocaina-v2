#!/bin/sh

pyinstaller src/main.py -n "CB_Software" \
    --add-data "src/images:images" \
    --distpath build/dist \
    --workpath build/build \
    --clean \
    -w \
    -i src/Logo_CaldinhoBocaina.ico