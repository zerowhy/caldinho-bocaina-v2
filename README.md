# caldinho-bocaina-v2
Versão 2 do software criado para impulsionar o desenvolvimento da Caldinho Bocaina. Base de dados em SQLite e interface gráfica (GUI) em PyQt5.

# Como executar
Este método é usado para executar o programa em ambiente de desenvolvimento.
Instale os requisitos no arquivo `requirements.txt`:
```bash
pip install -r requirements.txt
```

Entre na pasta `src/` e execute o script `gui.py`:
```bash
python main.py
```

Na primeira execução, será criado uma nova base de dados em SQLite na pasta onde seu terminal está localizado com nome `./database.db`.

No momento, não há uma maneira de criar itens do cardápio diretamente na interface gráfica. Para adicionar itens ao cardápio abra a base de dados em uma programa e adicione valores na tabela `cardapio`.

Para usar a função de pesquisar o endereço com o CEP dentro do programa, é necessário um token válido para consultas no [cepaberto.com](https://cepaberto.com).
Para isso, crie uma conta (é necessário e-mail) no site e navegue até [API](https://cepaberto.com/api_key) e encontre o seu token de acesso pessoal.
Com o token, crie um arquivo na pasta onde o executável se encontra chamado `token.txt` e cole o token, sem nenhum espaço a mais. Pronto!

# Build
Para compilar o programa e gerar um executável (para uso em produção), execute:

Linux:
```bash
git clone https://gitlab.com/zerowhy/caldinho-bocaina-v2 && cd caldinho-bocaina-v2
pip3 install -r requirements.txt
./build.sh
```
Windows:
```
git clone https://gitlab.com/zerowhy/caldinho-bocaina-v2
cd caldinho-bocaina-v2
pip install -r requirements.txt
.\build.bat
```

Após o processo finalizar, será gerado uma pasta `build` no diretório raiz do repositório.
O executável estará em:<br>
Windows: `build/dist/CB_Software/CB_Software.exe`<br>
Linux: `build/dist/CB_Software/CB_Software`

# Changelog
Veja o arquivo [CHANGELOG](https://gitlab.com/zerowhy/caldinho-bocaina-v2/-/blob/main/CHANGELOG.md)

# Licença
Esse software é distribuido sob a licença GPL-V3 ou qualquer versão mais recente.
Uma cópia da licença está disponível em [COPYING](https://gitlab.com/zerowhy/caldinho-bocaina-v2/-/blob/main/COPYING).
